package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Movie;
import com.example.demo.repository.MovieRepositories;

@Service("MovieService")
public class MovieServiceImpl implements MovieService{

	@Autowired
	private MovieRepositories movieRepositories;
	
	@Override
	public Iterable<Movie> findAll() {
		// TODO Auto-generated method stub
		return movieRepositories.findAll();
	}

	@Override
	public Movie findNewMovie() {
		// TODO Auto-generated method stub
		return movieRepositories.NewMovie();
	}

	@Override
	public Movie findMovieById(int movie_id) {
		// TODO Auto-generated method stub
		return movieRepositories.findMovieById(movie_id);
	}

	@Override
	public List<Movie> searchMovieName(String keyword) {
		// TODO Auto-generated method stub
		return movieRepositories.search(keyword);
	}

	@Override
	public Movie findMovieByName(String movieName) {
		// TODO Auto-generated method stub
		return movieRepositories.findMovieByName(movieName);
	}

}
