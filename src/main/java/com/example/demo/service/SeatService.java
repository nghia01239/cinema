package com.example.demo.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.example.demo.entities.Seat;

public interface SeatService {

	public Iterable<Seat> findAll();
	
	public List<Seat> findByRoomId(int room_id);

	public List<Seat> selectByRow(String row, int room_id);
	
	public Seat findSeatById(int seat_id);
}
