package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Cinema;
import com.example.demo.repository.CinemaRepository;

@Service("cinimeService")
public class CinemaServiceImpl implements CinemaService {

	@Autowired
	private CinemaRepository cinimeRepository;
	
	@Override
	public Iterable<Cinema> findAll() {
		// TODO Auto-generated method stub
		return cinimeRepository.findAll();
	}

}
