package com.example.demo.service;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import com.example.demo.entities.Schedule;

public interface ScheduleService {
	
	public Iterable<Schedule> findAll();
	
	public List<Schedule> findByMovieId(int movie_id, int n, String now);
	
	public List<Schedule> findByMovieAndRoom(int movie_id, int room_id, int n, String now);

	public Schedule findScheduleId(int schedule_id);
}
