package com.example.demo.service;

import java.util.List;

import com.example.demo.entities.Movie;

public interface MovieService {
	
	public Iterable<Movie> findAll();
	
	public Movie findNewMovie();
	
	public Movie findMovieById(int movie_id);
	
	public List<Movie> searchMovieName(String keyword);
	
	public Movie findMovieByName(String movieName);

}
