package com.example.demo.service;

import com.example.demo.entities.Room;

public interface RoomService {
	
	public Iterable<Room> findAll();

}
