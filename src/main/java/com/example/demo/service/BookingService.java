package com.example.demo.service;

import java.util.List;

import com.example.demo.entities.Booking;

public interface BookingService {

	public Iterable<Booking> findAll();
	
	public List<Booking> findByMovieId(int schedule_id);
	
	public List<Booking> findByUser(int user_id);
	
	public Booking save(Booking booking);
}
