package com.example.demo.service;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Schedule;
import com.example.demo.repository.ScheduleRepository;

@Service("scheduleService")
public class ScheduleServiceImpl implements ScheduleService {

	@Autowired
	private ScheduleRepository scheduleRepository;
	
	@Override
	public Iterable<Schedule> findAll() {
		// TODO Auto-generated method stub
		return scheduleRepository.findAll();
	}

	@Override
	public List<Schedule> findByMovieId(int movie_id, int n,String now) {
		// TODO Auto-generated method stub
		return scheduleRepository.findByMovieId(movie_id, n, now);
	}

	@Override
	public List<Schedule> findByMovieAndRoom(int movie_id, int room_id, int n, String now) {
		// TODO Auto-generated method stub
		return scheduleRepository.findByMovieAndRoom(movie_id, room_id, n, now);
	}

	@Override
	public Schedule findScheduleId(int schedule_id) {
		// TODO Auto-generated method stub
		return scheduleRepository.findByScheduleId(schedule_id);
	}

}
