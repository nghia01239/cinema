package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Booking;
import com.example.demo.repository.BookingRepository;

@Service("bookingService")
public class BookingServiceImpl implements BookingService{

	@Autowired
	private BookingRepository bookingRepository;
	
	@Override
	public Iterable<Booking> findAll() {
		// TODO Auto-generated method stub
		return bookingRepository.findAll();
	}

	@Override
	public List<Booking> findByMovieId(int schedule_id) {
		// TODO Auto-generated method stub
		return bookingRepository.findByMovieId(schedule_id);
	}

	@Override
	public Booking save(Booking booking) {
		// TODO Auto-generated method stub
		return bookingRepository.save(booking);
	}

	@Override
	public List<Booking> findByUser(int user_id) {
		// TODO Auto-generated method stub
		return bookingRepository.findByUserId(user_id);
	}

}
