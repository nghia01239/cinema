package com.example.demo.service;

import com.example.demo.entities.Users;

public interface UserService {
	
	public Iterable<Users> findAll();
	
	public Users save(Users users);

	public Users findUserById(int id);
	
	public Users findUserByName(String username);
}
