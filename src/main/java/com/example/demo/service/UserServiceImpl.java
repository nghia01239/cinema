package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Users;
import com.example.demo.repository.UsersRepositories;

@Service("userService")
public class UserServiceImpl implements UserService{

	@Autowired
	private UsersRepositories usersRepositories;
	
	@Override
	public Iterable<Users> findAll() {
		// TODO Auto-generated method stub
		return usersRepositories.findAll();
	}

	@Override
	public Users save(Users users) {
		// TODO Auto-generated method stub
		return usersRepositories.save(users);
	}

	@Override
	public Users findUserById(int id) {
		// TODO Auto-generated method stub
		return usersRepositories.findUserById(id);
	}

	@Override
	public Users findUserByName(String username) {
		// TODO Auto-generated method stub
		return usersRepositories.findUserByName(username);
	}

}
