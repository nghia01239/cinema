package com.example.demo.service;

import com.example.demo.entities.Cinema;

public interface CinemaService {

	public Iterable<Cinema> findAll();
}
