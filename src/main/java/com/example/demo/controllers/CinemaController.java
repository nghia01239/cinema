package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Cinema;
import com.example.demo.entities.Seat;
import com.example.demo.entities.Users;
import com.example.demo.service.CinemaService;
import com.example.demo.service.SeatService;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("api/cinema")
public class CinemaController {
	
	@Autowired
	private CinemaService service;
	
	@RequestMapping(value = "findall", method = RequestMethod.GET, 
			produces = MimeTypeUtils.APPLICATION_JSON_VALUE )
	public ResponseEntity<Iterable<Cinema> > findAll(){
		try {
			System.out.println("success!!");
			
			return new ResponseEntity<Iterable<Cinema>>(service.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Iterable<Cinema>>(HttpStatus.BAD_REQUEST);
		}
	}

}
