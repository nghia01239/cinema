package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Booking;
import com.example.demo.entities.Users;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("api/user")
public class UserController {
	
	@Autowired
	private UserService service;
	
	@RequestMapping(value = "findall", method = RequestMethod.GET, 
			produces = MimeTypeUtils.APPLICATION_JSON_VALUE )
	public ResponseEntity<Iterable<Users> > findAll(){
		try {
			System.out.println("success!!");
			
			return new ResponseEntity<Iterable<Users>>(service.findAll(), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Iterable<Users>>(HttpStatus.BAD_REQUEST);
		}
	}

	
	@RequestMapping(value = "findById/{id}", method = RequestMethod.GET, 
			produces = MimeTypeUtils.APPLICATION_JSON_VALUE )
	public ResponseEntity<Users> findById(@PathVariable("id") int id){
		try {
			System.out.println("success!!");
			
			return new ResponseEntity<Users>(service.findUserById(id), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Users>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = "findByUsername/{username}", method = RequestMethod.GET, 
			produces = MimeTypeUtils.APPLICATION_JSON_VALUE )
	public ResponseEntity<Users> findByName(@PathVariable("username") String username){
		try {
			System.out.println("success!!");
			
			return new ResponseEntity<Users>(service.findUserByName(username), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Users>(HttpStatus.BAD_REQUEST);
		}
	}

	//register
	@RequestMapping(value = "register", method = RequestMethod.POST,
	consumes = MimeTypeUtils.APPLICATION_JSON_VALUE,
			produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
	public ResponseEntity<Users> create(@RequestBody Users users){
		try {
			
			return new ResponseEntity<Users>(service.save(users), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Users>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
}
