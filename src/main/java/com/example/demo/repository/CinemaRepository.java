package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Cinema;

@Repository("CinemaRepositories")
public interface CinemaRepository extends CrudRepository<Cinema, Integer> {

}
