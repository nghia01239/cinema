package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import com.example.demo.entities.Movie;

@Repository("MovieRepositories")
public interface MovieRepositories extends CrudRepository<Movie, Integer> {

	@Query(value = "select * from Movie order by movie_id desc limit 0,1", nativeQuery = true)
	public Movie NewMovie();
	
	@Query(value = "select * from Movie where movie_id = :movie_id", nativeQuery = true)
	public Movie findMovieById(@Param("movie_id") int movie_id);
	
	@Query(value = "select * from Movie where movie_name = :movie_name", nativeQuery = true)
	public Movie findMovieByName(@Param("movie_name") String movieName);
	
	@Query(value ="SELECT * FROM Movie where movie_name like %:keyword%", nativeQuery = true)
	public List<Movie> search(@Param("keyword") String keyword);
}
