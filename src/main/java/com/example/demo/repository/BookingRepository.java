package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Booking;

@Repository("BookingRepositories")
public interface BookingRepository extends CrudRepository<Booking, Integer>{
	@Query(value = "select * from Booking b where schedule_id = :schedule_id order by b.booking_id asc", nativeQuery = true)
	public List<Booking> findByMovieId(@Param("schedule_id") int schedule_id);
	
	@Query(value = "select * from Booking b where user_id = :user_id order by b.booking_id asc", nativeQuery = true)
	public List<Booking> findByUserId(@Param("user_id") int user_id);
	 
}
