package com.example.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Users;

@Repository("UserRepositories")
public interface UsersRepositories extends CrudRepository<Users, Integer> {

	@Query("from Users where id = :id")
	public Users findUserById(@Param("id") int id);
	
	@Query("from Users where username = :username")
	public Users findUserByName(@Param("username") String username);
	
}
