package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Room;

@Repository("RoomRepositories")
public interface RoomRepository extends CrudRepository<Room, Integer> {

}
